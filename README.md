# RAE359 #

## Distributed Systems ##

### Links:
*[Google](https://www.google.lv)

*[Safari Books](https://www.safaribooksonline.com/)

*[Distance Education Centre](http://tsc.edx.lv/)

*[Distributed Systems in One Lesson](https://www.safaribooksonline.com/library/view/distributed-systems-in/9781491924914/)

[Atskats uz kursu "Komunikācijas distributīvās sistēmas"](http://www.youtube.com/watch?v=pLt5qsNMhks)

Ar [Vitālijs Devjatovskis](https://bitbucket.org/vitalijs_devjatovskis)
un [Ņikita Krupeņins](https://bitbucket.org/151REB044)

[Videolekcijas pārskats](https://youtu.be/IVZIlQEfYnc)